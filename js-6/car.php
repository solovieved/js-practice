<?
$arCars = ['bmw', 'mercedes', 'audi', 'skoda', 'volvo', 'reno', 'toyota', 'mazda'];
$count = count($arCars);
$value = $_GET['number'];
$newArCars = array_slice($arCars, 0, $value);
if ($value > $count) {
    print '<p>Недопустимое значение</p>';
    exit;
}

foreach ($newArCars as $car) {
    print '<li>' . $car . '</li>';
}